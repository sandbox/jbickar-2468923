<?php
/**
 * @file
 * camp_sessions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function camp_sessions_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_location'.
  $permissions['create field_location'] = array(
    'name' => 'create field_location',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_s_day'.
  $permissions['create field_s_day'] = array(
    'name' => 'create field_s_day',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_s_session_status'.
  $permissions['create field_s_session_status'] = array(
    'name' => 'create field_s_session_status',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_s_time'.
  $permissions['create field_s_time'] = array(
    'name' => 'create field_s_time',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create stanford_session content'.
  $permissions['create stanford_session content'] = array(
    'name' => 'create stanford_session content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any stanford_session content'.
  $permissions['delete any stanford_session content'] = array(
    'name' => 'delete any stanford_session content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own stanford_session content'.
  $permissions['delete own stanford_session content'] = array(
    'name' => 'delete own stanford_session content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any stanford_session content'.
  $permissions['edit any stanford_session content'] = array(
    'name' => 'edit any stanford_session content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit field_location'.
  $permissions['edit field_location'] = array(
    'name' => 'edit field_location',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_s_day'.
  $permissions['edit field_s_day'] = array(
    'name' => 'edit field_s_day',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_s_session_status'.
  $permissions['edit field_s_session_status'] = array(
    'name' => 'edit field_s_session_status',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_s_time'.
  $permissions['edit field_s_time'] = array(
    'name' => 'edit field_s_time',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_location'.
  $permissions['edit own field_location'] = array(
    'name' => 'edit own field_location',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_s_day'.
  $permissions['edit own field_s_day'] = array(
    'name' => 'edit own field_s_day',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_s_session_status'.
  $permissions['edit own field_s_session_status'] = array(
    'name' => 'edit own field_s_session_status',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_s_time'.
  $permissions['edit own field_s_time'] = array(
    'name' => 'edit own field_s_time',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own stanford_session content'.
  $permissions['edit own stanford_session content'] = array(
    'name' => 'edit own stanford_session content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'flag add_session_to_my_schedule'.
  $permissions['flag add_session_to_my_schedule'] = array(
    'name' => 'flag add_session_to_my_schedule',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag add_session_to_my_schedule'.
  $permissions['unflag add_session_to_my_schedule'] = array(
    'name' => 'unflag add_session_to_my_schedule',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'view field_location'.
  $permissions['view field_location'] = array(
    'name' => 'view field_location',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_s_day'.
  $permissions['view field_s_day'] = array(
    'name' => 'view field_s_day',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_s_session_status'.
  $permissions['view field_s_session_status'] = array(
    'name' => 'view field_s_session_status',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_s_time'.
  $permissions['view field_s_time'] = array(
    'name' => 'view field_s_time',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_location'.
  $permissions['view own field_location'] = array(
    'name' => 'view own field_location',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_s_day'.
  $permissions['view own field_s_day'] = array(
    'name' => 'view own field_s_day',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_s_session_status'.
  $permissions['view own field_s_session_status'] = array(
    'name' => 'view own field_s_session_status',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_s_time'.
  $permissions['view own field_s_time'] = array(
    'name' => 'view own field_s_time',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
