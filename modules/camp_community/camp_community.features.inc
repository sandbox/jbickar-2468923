<?php
/**
 * @file
 * camp_community.features.inc
 */

/**
 * Implements hook_views_api().
 */
function camp_community_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
